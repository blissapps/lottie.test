  //
//  LottieAnimationViewController.swift
//  lottie.test
//
//  Created by João Campos on 02/05/2017.
//  Copyright © 2017 blissapps. All rights reserved.
//

import UIKit
import Lottie

class LottieAnimationViewController: UIViewController {

  @IBOutlet weak var animatedView: UIView!
  var url:String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    if let url = url, url != "" {
      let path = URL(string: url)
      let animationView = LOTAnimationView.init(contentsOf: path!)
      animationView?.frame = CGRect(x: 0, y: 0, width: self.animatedView.frame.size.width, height: self.animatedView.frame.size.height)
      self.animatedView.addSubview(animationView!)

      animationView?.play()
    }
  }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

  @IBAction func btnBackPressed(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
