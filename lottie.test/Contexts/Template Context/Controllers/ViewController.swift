//
//  ViewController.swift
//  {PROJECT}
//
//  Created by João Campos on 05/02/2017.
//  Copyright © 2017 blissapps. All rights reserved.
//

import UIKit

final class ViewController: UIViewController {

  @IBOutlet weak var textFieldUrl: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
  @IBAction func btnShowAnimation(_ sender: Any) {
    let lottieVC = LottieAnimationViewController()
    lottieVC.url = textFieldUrl.text
    present(lottieVC, animated: true, completion: nil)
  }
}

